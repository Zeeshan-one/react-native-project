
import {
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {colors} from "./src/util/Color"
import Focus from './src/features/Focus';
import { RoundedButton } from './src/components/RoundedButton';
import { useState } from 'react';
const App = () => {
  let[edit,setEdit]=useState(false)
  return (
    <SafeAreaView style={styles.Container}>
      <ScrollView style={styles.Container}>
        <View style={styles.textInput}>
        <Focus style={styles.text} editable={edit}/>
        <RoundedButton size={50} title={"+"} onPress={(()=>setEdit(true))} />
        </View>
        </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  Container: {
    backgroundColor:colors.darkBlue,
    flex:1,
  },
  textInput:{
    flexDirection:"row",
    alignItems:"center"
  },
   text:{
    color:colors.darkBlue,
    fontSize:30
   }
});

export default App;
