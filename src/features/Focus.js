import { StyleSheet, Text, TextInput, View } from 'react-native'
import React, { useState } from 'react'
import { colors } from '../util/Color'

const Focus = (props) => {
    let [text,setText]=useState("")
  return (
    <View style={styles.Container}>
      <TextInput  style={props.style} value={text}  onChangeText={setText}  multiline  placeholder={"any thing to focus"} allowFontScaling={true}  onFocus={()=>console.log("in focusss")} onBlur={()=>console.log("in blirrrr")}/>
    </View>
  )
}

const styles = StyleSheet.create({
    Container:{
     flex:1,
     backgroundColor:colors.white,
     borderBottomColor:"black",
     borderBottomWidth:5,
     margin:5
    },
 })
export default Focus

